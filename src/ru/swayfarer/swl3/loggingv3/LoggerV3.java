package ru.swayfarer.swl3.loggingv3;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.api.logger.ILogger;
import ru.swayfarer.swl3.api.logger.ILoggerFactory;
import ru.swayfarer.swl3.api.logger.LogFactory;
import ru.swayfarer.swl3.api.logger.LogHelper;
import ru.swayfarer.swl3.api.logger.builder.LogBuilder;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.loggingv3.config.LoggerAutoConfigurator;
import ru.swayfarer.swl3.loggingv3.content.LogContent;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.info.LoggerInfo;
import ru.swayfarer.swl3.loggingv3.level.ILogLevel;
import ru.swayfarer.swl3.loggingv3.level.ILogLevels;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class LoggerV3 implements ILogger, ILogLevels {

    @NonNull
    public IObservable<LogEvent> eventFiltering = Observables.createObservable();

    @NonNull
    public IObservable<LogEvent> eventPreprocess = Observables.createObservable();
    
    @NonNull
    public IObservable<LogEvent> eventProcess = Observables.createObservable();
    
    @NonNull
    public IObservable<LogEvent> eventPostprocess = Observables.createObservable();
    
    @NonNull
    public LoggerInfo loggerInfo;

    public int globalSourceOffset;

    public LoggerV3(@NonNull LoggerInfo loggerInfo)
    {
        this.loggerInfo = loggerInfo;
    }

    @Override
    public void dev(@NonNull Object... text)
    {
        var event = createEvent(LEVEL_DEV, null, text);
        event.updateSource(1 + globalSourceOffset);
        log(event);
    }

    @Override
    public void info(@NonNull Object... text)
    {
        var event = createEvent(LEVEL_INFO, null, text);
        event.updateSource(1 + globalSourceOffset);
        log(event);
    }

    @Override
    public void warn(@NonNull Object... text)
    {
        var event = createEvent(LEVEL_WARNING, null, text);
        event.updateSource(1 + globalSourceOffset);
        log(event);
    }

    @Override
    public void error(@NonNull Object... text)
    {
        var event = createEvent(LEVEL_ERROR, null, text);
        event.updateSource(1 + globalSourceOffset);
        log(event);
    }

    @Override
    public void dev(@NonNull Throwable e, @NonNull Object... text)
    {
        var event = createEvent(LEVEL_DEV, e, text);
        event.updateSource(1 + globalSourceOffset);
        log(event);
    }

    @Override
    public void info(@NonNull Throwable e, @NonNull Object... text)
    {
        var event = createEvent(LEVEL_INFO, e, text);
        event.updateSource(1 + globalSourceOffset);
        log(event);
    }

    @Override
    public void warn(@NonNull Throwable e, @NonNull Object... text)
    {
        var event = createEvent(LEVEL_WARNING, e, text);
        event.updateSource(1 + globalSourceOffset);
        log(event);
    }

    @Override
    public void error(@NonNull Throwable e, @NonNull Object... text)
    {
        var event = createEvent(LEVEL_ERROR, e, text);
        event.updateSource(1 + globalSourceOffset);
        log(event);
    }

    @Override
    public LogBuilder log()
    {
        return new LogBuilder(this::processBuilder);
    }

    public void processBuilder(LogBuilder builder)
    {
        var logLevel = builder.logLevel;
        var throwable = builder.throwable;
        var logV3Level = LogHelper.findLogLevel(logLevel, LEVEL_DEV, LEVEL_INFO, LEVEL_WARNING, LEVEL_ERROR);
        var event = createEvent(logV3Level, throwable, null);

        event.source = builder.source;
        event.customAttributes.putAll(builder.customAttributes);
        event.content.contentStr = builder.contentFun;

        log(event);
    }

    public void log(@NonNull LogEvent event)
    {
        eventFiltering.next(event);

        if (event.isCanceled())
            return;

        eventPreprocess.next(event);

        if (event.isCanceled())
            return;

        eventProcess.next(event);

        if (event.isCanceled())
            return;

        eventPostprocess.next(event);
    }
    
    public StackTraceElement getSource()
    {
        return getLoggerInfo().getSource().getSource();
    }
    
    public LogEvent createEvent(@NonNull ILogLevel level, Throwable e, Object... text)
    {
        var content = new LogContent(ThreadsUtils.singleton(() -> StringUtils.concatWithSpaces(text)));
        
        if (e != null)
        {
            content.setThrowable(e);
        }
        
        var event = LogEvent.builder()
            .thread(Thread.currentThread())
            .logger(this)
            .content(content)
            .level(level)
            .build()
        ;
        
        return event;
    }
    
    public static class LogFactoryV3 {
        
        @NonNull
        public static IFunction0<LoggerAutoConfigurator> autoConfigurator = 
            ThreadsUtils.singleton(() -> new LoggerAutoConfigurator().load())
        ;
        
        public static LoggerV3 autoConfigure(@NonNull LoggerV3 logger)
        {
            return autoConfigurator.apply().configure(logger);
        }
        
        public static LoggerV3 getLogger()
        {
            var loggerInfo = new LoggerInfo();
            loggerInfo.setName(ReflectionsUtils.getClassSimpleName(ExceptionsUtils.caller().getClassName()));
            
            var ret = new LoggerV3(loggerInfo);
            
            loggerInfo.updateSource(1);
            return LogFactoryV3.autoConfigure(ret);
        }
        
        public static LoggerV3 getLogger(@NonNull Object loggerName)
        {
            var loggerInfo = new LoggerInfo();
            loggerInfo.setName(loggerName.toString());
            
            var ret = new LoggerV3(loggerInfo);
            
            loggerInfo.updateSource(1);
            return LogFactoryV3.autoConfigure(ret);
        }

        public static LoggerV3 getLogger(@NonNull Object loggerName, int sourcesOffset, int logsOffset)
        {
            var loggerInfo = new LoggerInfo();
            loggerInfo.setName(loggerName.toString());

            var ret = new LoggerV3(loggerInfo);

            loggerInfo.updateSource(1 + sourcesOffset);
            ret.setGlobalSourceOffset(logsOffset);

            return LogFactoryV3.autoConfigure(ret);
        }


        public static void useAsDefaultSwl3Logger()
        {
            LogFactory.loggerFun = getLoggerV3Factory();
        }

        public static ILoggerFactory getLoggerV3Factory()
        {
            return (logCreatingEvent) -> {
                return getLogger(logCreatingEvent.getName(), logCreatingEvent.getSourceOffset() + 2, logCreatingEvent.getLogOffset());
            };
        }
    }
}
