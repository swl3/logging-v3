package ru.swayfarer.swl3.loggingv3;

import ru.swayfarer.swl3.api.logger.ILoggerFactory;
import ru.swayfarer.swl3.api.logger.ILoggerFactoryProvider;
import ru.swayfarer.swl3.loggingv3.LoggerV3.LogFactoryV3;

public class LoggerV3CreationProvider implements ILoggerFactoryProvider
{
    @Override
    public String getId()
    {
        return "logging-v3";
    }

    @Override
    public ILoggerFactory getLoggerFactory()
    {
        return LogFactoryV3.getLoggerV3Factory();
    }
}
