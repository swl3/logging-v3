package ru.swayfarer.swl3.loggingv3.coloring;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.loggingv3.coloring.Palette.PaletteColor;

@Data
@Accessors(chain = true)
public class ColoringManager {
    
    @NonNull
    public PaletteColor blankColor = PaletteColor.builder()
        .colorPrefix("\033[0m")
        .ansiAnalogName("blank")
        .build()
    ;
    
    @NonNull
    public PaletteRegistry paletteRegistry = new PaletteRegistry();
    
    public ColoringManager registerPalette(@NonNull String name, @NonNull Palette palette)
    {
        this.paletteRegistry.registerPalette(name, palette);
        return this;
    }
    
    public ColoringManager makeAnsiDefaults()
    {
        var ansiPalette = paletteRegistry.getPalette(DefaultPalettes.PALETTE_ANSI_NAME);
        
        for (var registeredPalette : paletteRegistry.getRegisteredPalettes()) 
        {
            var paletteKey = registeredPalette.getKey();
            var palette = registeredPalette.getValue();
            
            if (!paletteKey.equalsIgnoreCase("ansi"))
            {
                for (var color : palette.getColors().copy())
                {
                    var keys = color.getKeys();
                    
                    var ansiAnalogName = color.getAnsiAnalogName();
                    var colorPrefix = ansiPalette.findColor(ansiAnalogName).orElse(blankColor).getColorPrefix();
                    
                    ansiPalette.colorWithANSI (
                            ansiAnalogName, 
                            colorPrefix, 
                            keys.toArray(String.class)
                    );
                }
            }
        }
        
        return this;
    }
    
    public String getColorPrefix(@NonNull String paletteName, @NonNull String... names)
    {
        var anciPalette = paletteRegistry.getPalette(DefaultPalettes.PALETTE_ANSI_NAME);
        var palette = paletteRegistry.getPalette(paletteName);
        
        if (palette == null) 
        {
            palette = anciPalette ;
            
            if (palette == null)
                return null;
        }
        
        var color = palette.findColor(names).orElse(anciPalette.findColor(names).orElse(blankColor));
        
        return color.getColorPrefix();
    }
}
