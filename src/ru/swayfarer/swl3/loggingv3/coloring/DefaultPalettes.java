package ru.swayfarer.swl3.loggingv3.coloring;

import lombok.var;
import ru.swayfarer.swl3.io.link.RLUtils;

public interface DefaultPalettes {
    
    public String PALETTES_LOCATION = RLUtils.fromThisClass("/");
    public PaletteIO PALETTE_IO = new PaletteIO();
    
    public String PALETTE_ANSI_NAME = "ANSI";
    public String PALETTE_8BIT_NAME = "8BIT";

    public Palette PALETTE_8BIT = PALETTE_IO.read(PALETTES_LOCATION + "8bitPalette.yml");
    public Palette PALETTE_ANSI = PALETTE_IO.read(PALETTES_LOCATION + "ansiPalette.yml");
    
    public static ColoringManager registerDefaults(ColoringManager coloringManager)
    {
        if (coloringManager == null)
            return null;
        
        coloringManager.registerPalette(PALETTE_ANSI_NAME, PALETTE_ANSI);
        coloringManager.registerPalette(PALETTE_8BIT_NAME, PALETTE_8BIT);

        return coloringManager;
    }
}
