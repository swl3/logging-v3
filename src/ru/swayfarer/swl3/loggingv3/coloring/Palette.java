package ru.swayfarer.swl3.loggingv3.coloring;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.ExtendedOptional;

@Data
@Accessors(chain = true)
public class Palette {
    
    public ExtendedList<PaletteColor> colors = new ExtendedList<>();
    
    public Palette color(@NonNull String colorPrefix, @NonNull String... keys)
    {
        return colorWithANSI(colorPrefix, colorPrefix, keys);
    }
    
    public Palette colorWithANSI(@NonNull String ansiAnalogName, @NonNull String colorPrefix, @NonNull String... keys)
    {
        var color = PaletteColor.builder()
                .ansiAnalogName(ansiAnalogName)
                .colorPrefix(colorPrefix)
                .keys(CollectionsSWL.list(keys))
                .build()
        ;
        
        colors.add(color);
        
        return this;
    }
    
    public ExtendedOptional<PaletteColor> findColor(@NonNull String... keys)
    {
        var keysIterable = CollectionsSWL.iterable(keys);
        
        return colors.exStream()
                .filter((e) -> e.getKeys().containsSome(keysIterable))
                .findFirst()
        ;
    }
    
    public Palette copy()
    {
        var ret = new Palette();
        colors.each((e) -> ret.colors.add(e.copy()));
        return this;
    }
    
    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    @Accessors(chain = true)
    public static class PaletteColor {
        
        @Builder.Default
        @NonNull
        public ExtendedList<String> keys = new ExtendedList<>();
        
        @NonNull
        public String colorPrefix;
        
        @NonNull
        public String ansiAnalogName;
        
        public PaletteColor copy()
        {
            return new PaletteColor()
                    .setAnsiAnalogName(ansiAnalogName)
                    .setColorPrefix(colorPrefix)
                    .setKeys(keys.copy())
            ;
        }
    }
}
