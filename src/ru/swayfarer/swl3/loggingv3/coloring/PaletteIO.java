package ru.swayfarer.swl3.loggingv3.coloring;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.swconf2.helper.SwconfIO;

public class PaletteIO {
    
    public RLUtils rlutils = RLUtils.defaultUtils();
    public SwconfIO swconfSerialization = new SwconfIO();
    
    public Palette read(@NonNull String rlink)
    {
        if (rlink.equalsIgnoreCase("<ansi>"))
            return DefaultPalettes.PALETTE_ANSI.copy();
        else if (rlink.equalsIgnoreCase("<8bit>"))
            return DefaultPalettes.PALETTE_8BIT.copy();
        
        var readedPalette = swconfSerialization.deserialize(Palette.class, rlutils.createLink(rlink));
        
        for (var color : readedPalette.getColors()) 
        {
            color.colorPrefix = color.colorPrefix.replace("^", "\u001B");
        }
        
        return readedPalette;
    }
    
    public PaletteIO save(@NonNull Palette palette, @NonNull String rlink)
    {
        swconfSerialization.serialize(palette, rlutils.createLink(rlink));
        return this;
    }
}
