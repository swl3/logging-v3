package ru.swayfarer.swl3.loggingv3.coloring;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;

@Getter
@Setter
@Accessors(chain = true)
public class PaletteRegistry {
    
    @NonNull
    public ExtendedMap<String, Palette> registeredPalettes = new ExtendedMap<>().synchronize();
    
    public Palette getPalette(@NonNull String name)
    {
        return registeredPalettes.get(name);
    }
    
    public PaletteRegistry registerPalette(@NonNull String name, @NonNull Palette palette)
    {
        this.registeredPalettes.put(name, palette);
        return this;
    }
}
