package ru.swayfarer.swl3.loggingv3.config;

import lombok.var;
import java.util.concurrent.TimeUnit;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.io.BinaryUtils;
import ru.swayfarer.swl3.loggingv3.event.io.LogFileWriterHandler;
import ru.swayfarer.swl3.loggingv3.io.StandartLogFileArchiveConditions;
import ru.swayfarer.swl3.observable.property.DateTimeUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;

@Data
@Accessors(chain = true)
public class ArchiveCfg {
    
    @CommentedSwconf
    public String pattern = "logs/archive/%logFileName%.gz";
    
    @CommentedSwconf
    public String maxSize;
    
    @CommentedSwconf
    public String time;
    
    public FunctionsChain1<LogFileWriterHandler, Boolean> asArchiveConditionFun(@NonNull BinaryUtils binaryUtils, @NonNull DateTimeUtils timeUtils)
    {
        var ret = new FunctionsChain1<LogFileWriterHandler, Boolean>();
        ret.setFilterFun((e) -> Boolean.TRUE.equals(e));
        
        if (!StringUtils.isBlank(maxSize))
        {
            ret.add(StandartLogFileArchiveConditions.maxFileSizeCondition(binaryUtils.bytesOf(maxSize)));
        }
        
        if (!StringUtils.isBlank(time))
        {
            ret.add(StandartLogFileArchiveConditions.delayCondition(timeUtils.milisecondsOf(time), TimeUnit.MILLISECONDS));
        }
        
        return ret;
    }
}
