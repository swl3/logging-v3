package ru.swayfarer.swl3.loggingv3.config;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.loggingv3.coloring.ColoringManager;
import ru.swayfarer.swl3.loggingv3.coloring.DefaultPalettes;
import ru.swayfarer.swl3.loggingv3.coloring.PaletteIO;
import ru.swayfarer.swl3.loggingv3.event.formatting.IHasInsertionRules;
import ru.swayfarer.swl3.loggingv3.formatting.StandardInsertionsPresets;
import ru.swayfarer.swl3.loggingv3.formatting.insertion.ColoringInsertionHandler;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl3.system.SystemUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ColoringCfg {
    
    @CommentedSwconf
    public String defaultPalette;

    @CommentedSwconf
    public ExtendedMap<String, String> palettesPaths = new ExtendedMap<>();
    
    @CommentedSwconf
    public ExtendedMap<String, String> levelColors = new ExtendedMap<>();

    @CommentedSwconf
    public boolean enable = true;


    /*
     * -----------------------------------------------------------*
     *                      Apply Code                            *
     * -----------------------------------------------------------*
     */

    @Tolerate
    public ColoringCfg setPalettesPaths(@NonNull Object... path)
    {
        ExtendedMap<String, String> map = CollectionsSWL.exMap(path);
        return setPalettesPaths(map);
    }
    
    public ColoringCfg apply(@NonNull LoggerV3 logger)
    {
        if (!enable)
            return this;

        SystemUtils.getInstance().tryToEnableConsoleColors();

        var paletteIO = new PaletteIO();
        var colMgr = new ColoringManager();
        
        if (CollectionsSWL.isNullOrEmpty(palettesPaths))
        {
            DefaultPalettes.registerDefaults(colMgr);
        }
        else
        {
            for (var paletteEntry : palettesPaths)
            {
                var name = paletteEntry.getKey();
                var path = paletteEntry.getValue();
                var palette = paletteIO.read(path);
                
                if (palette != null)
                {
                    colMgr.registerPalette(name, palette);
                }
            }
        }
        
        colMgr.makeAnsiDefaults();
        
        var colHandler = new ColoringInsertionHandler(colMgr, defaultPalette);

        if (!CollectionsSWL.isNullOrEmpty(levelColors))
            colHandler.getLevelColors().putAll(levelColors);

        postProcessInsertions(logger, colHandler);

        return this;
    }
    
    public LoggerV3 postProcessInsertions(@NonNull LoggerV3 logger, @NonNull ColoringInsertionHandler colHandler)
    {
        var event = logger.eventPreprocess;
        var insertionsAcceptors = event.tasks()
                .findTaskObjects(IHasInsertionRules.class)
        ;

        var insertionRule = StandardInsertionsPresets.replace(colHandler::apply, "color", "col");

        for (var acceptor : insertionsAcceptors)
        {
            acceptor.addInsertionRule(insertionRule);
        }
        
        return logger;
    }
}
