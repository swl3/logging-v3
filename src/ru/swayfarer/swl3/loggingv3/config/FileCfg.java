package ru.swayfarer.swl3.loggingv3.config;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.io.BinaryUtils;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.loggingv3.event.io.LogFileWriterHandler;
import ru.swayfarer.swl3.loggingv3.io.StandartLogFileArchivers;
import ru.swayfarer.swl3.observable.property.DateTimeUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;

@Getter
@Setter
@Accessors(chain = true)
public class FileCfg {
    
    @CommentedSwconf
    public FilteringCfg filter;
    
    @CommentedSwconf
    public String pattern;
    
    @CommentedSwconf
    public ArchiveCfg archive;
    
    public LoggerV3 apply(@NonNull DateTimeUtils timeUtils, @NonNull BinaryUtils binaryUtils, @NonNull LoggerV3 logger)
    {
        if (!StringUtils.isBlank(pattern))
        {
            var handler = new LogFileWriterHandler(pattern);
            
            if (archive != null)
            {
                var archiveCondition = archive.asArchiveConditionFun(binaryUtils, timeUtils);
                handler.setArchiveConditionsFuns(archiveCondition);

                var archiverFun = StandartLogFileArchivers.toFile(
                      (f) -> f.out().to().gz(), 
                      archive.getPattern()
                );
                
                handler.eventArchive.subscribe().by(archiverFun);
                handler.eventArchive.subscribe().by(StandartLogFileArchivers.removeFile());
            }
            
            ILogHandler handlerToSubscribe = handler;
            
            if (filter != null)
            {
                var filters = filter.getFilters();
                
                if (filters.funs.isNotEmpty())
                {
                    handlerToSubscribe = (evt) -> {
                        var filterResult = filters.apply(evt);
                        
                        if (Boolean.TRUE.equals(filterResult))
                        {
                            handler.apply(evt);
                        }
                    };
                }
            }
            
            logger.eventPostprocess.subscribe().by(handlerToSubscribe);
        }
        
        return logger;
    }
}
