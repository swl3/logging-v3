package ru.swayfarer.swl3.loggingv3.config;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.filtering.LogFilteringHandler;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.loggingv3.filtering.LogStacktraceFilter;
import ru.swayfarer.swl3.loggingv3.filtering.StandartLogFilters;
import ru.swayfarer.swl3.string.ExpressionsList;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;

@Getter
@Setter
@Accessors(chain = true)
public class FilteringCfg {
    
    @CommentedSwconf
    public SourceFiltering sources;
    
    @CommentedSwconf
    public Integer maxLevel;
    
    @CommentedSwconf
    public Integer minLevel;
    
    @CommentedSwconf
    public ExtendedList<String> levels;
    
    @CommentedSwconf
    public ExtendedList<String> content;

    @CommentedSwconf
    public ExtendedList<String> stacktrace;
    
    public void apply(@NonNull LoggerV3 logger)
    {
        var filteringHandler = new LogFilteringHandler();
        var filteringFuns = filteringHandler.getFilterFuns();
        addFilters(filteringFuns);
        logger.eventFiltering.subscribe().by(filteringHandler);

        if (!CollectionsSWL.isNullOrEmpty(stacktrace))
        {
            var stackTraceFilter = new LogStacktraceFilter();
            stackTraceFilter.getFilters().addAll(stacktrace);
            logger.eventPreprocess.subscribe().by(stackTraceFilter);
        }
    }
    
    public FunctionsChain1<LogEvent, Boolean> getFilters()
    {
        var funs = new FunctionsChain1<LogEvent, Boolean>();
        addFilters(funs);
        return funs;
    }
    
    public FilteringCfg addFilters(@NonNull FunctionsChain1<LogEvent, Boolean> funs)
    {
        if (sources != null)
        {
            var sourcesFun = sources.asFilterFun();
            
            funs.add(StandartLogFilters.bySource(sourcesFun));
        }
        
        if (maxLevel != null)
        {
            funs.add(StandartLogFilters.maxLevel(maxLevel));
        }
        
        if (minLevel != null)
        {
            funs.add(StandartLogFilters.minLevel(minLevel));
        }
        
        if (levels != null)
        {
            funs.add(StandartLogFilters.levelExpressions(levels));
        }
        
        if (content != null)
        {
            funs.add(StandartLogFilters.contentExpressions(content));
        }
        
        return this;
    }
    
    public static ILogHandler filtered(FilteringCfg filteringCfg, @NonNull ILogHandler handler)
    {
        if (filteringCfg == null)
        {
            return handler;
        }
        
        var filters = filteringCfg.getFilters();
        
        if (filters == null || filters.funs.isEmpty())
        {
            return handler;
        }
        
        return (evt) -> {
            if (Boolean.TRUE.equals(filters.apply(evt)))
            {
                handler.handle(evt);
            }
        };
    }
}
