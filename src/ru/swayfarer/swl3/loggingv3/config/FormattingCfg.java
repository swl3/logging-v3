package ru.swayfarer.swl3.loggingv3.config;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.loggingv3.event.formatting.FormattingLogHandler;
import ru.swayfarer.swl3.loggingv3.formatting.StandartFormatting;
import ru.swayfarer.swl3.loggingv3.info.LogFormattingInfo;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;

@Getter
@Setter
@Accessors(chain = true)
public class FormattingCfg {
    
    @CommentedSwconf
    public String log;

    @CommentedSwconf
    public String stacktraceHeader;
    
    @CommentedSwconf
    public String stacktrace;
    
    @CommentedSwconf
    public boolean useFastFormatter = true;
    
    public LoggerV3 apply(@NonNull LoggerV3 logger)
    {
        if (!StringUtils.isBlank(log))
        {
            var logFormattingInfo = new LogFormattingInfo(log);
            var logFormatter = new FormattingLogHandler(logFormattingInfo);
            
            var insertionsFormatterRule = StandartFormatting.getInsertionLogFormatterRule();
            
            logFormatter.eventFormatting.subscribe().by(insertionsFormatterRule);
            
            logger.eventPreprocess.subscribe().by(logFormatter);
        }
        
        if (!StringUtils.isBlank(stacktrace))
        {
            var stacktraceFormatter = StandartFormatting.getStacktraceFormatter(stacktrace);
            logger.eventPreprocess.subscribe().by(stacktraceFormatter);
        }

        if (!StringUtils.isBlank(stacktraceHeader))
        {
            var headerFormatter = StandartFormatting.getExceptionsHeaderFormatter(stacktraceHeader);
            logger.eventPreprocess.subscribe().by(headerFormatter);
        }
        
        return logger;
    }
}
