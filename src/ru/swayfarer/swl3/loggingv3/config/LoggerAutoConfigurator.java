package ru.swayfarer.swl3.loggingv3.config;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.io.link.RLUtils;
import ru.swayfarer.swl3.io.link.ResourceLink;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.loggingv3.exception.LoggingInitializationException;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.StringValue;
import ru.swayfarer.swl3.swconf2.helper.SwconfIO;
import ru.swayfarer.swl3.system.SystemUtils;

import java.util.LinkedHashMap;

@Getter
@Setter
@Accessors(chain = true)
public class LoggerAutoConfigurator {

    public static String fallbackConfigLocation = RLUtils.fromThisClass("/fallback-logging-v3.yml");

    public String defaultConfigLocation = "logging-v3.yml";
    public String configLocationPropertyName = "swl3.loggingv3.config.loc";

    public StringValue configLocationProperty;

    public LoggerCfg loggerCfg;

    public SystemUtils systemUtils = SystemUtils.getInstance();
    public SwconfIO swconfIO = SwconfIO.getInstance();
    public RLUtils rlUtils = RLUtils.getInstance();

    public LoggerV3 configure(@NonNull LoggerV3 logger)
    {
        if (loggerCfg != null)
        {
            loggerCfg.apply(logger);
        }

        return logger;
    }

    public LoggerAutoConfigurator load()
    {
        try
        {
            configLocationProperty = systemUtils.findProperty(configLocationPropertyName, defaultConfigLocation);

            var rlink = configLocationProperty.getValue(ResourceLink.class);

            if (rlink != null)
            {
                if (rlink.isExists())
                {
                    loggerCfg = readLoggerCfg(rlink);
                }
                else
                {
                    if (fallbackConfigLocation != null)
                    {
                        var fallbackConfigLoc = rlUtils.createLink(fallbackConfigLocation);

                        if (fallbackConfigLoc.isExists())
                        {
                            loggerCfg = readLoggerCfg(fallbackConfigLoc);
                            System.err.println("Warn: LoggerV3 configuration was not found at '" + configLocationProperty.getStringValue() + "', using fallback!");
                        }
                    }
                    else
                    {
                        System.err.println("Warn: LoggerV3 configuration was not found at '" + configLocationProperty.getStringValue() + "'");
                    }
                }
            }
        }
        catch (Throwable e)
        {
            throw new LoggingInitializationException("Error while initializing logging config at " + configLocationProperty.getStringValue(), e);
        }

        return this;
    }

    private LoggerCfg readLoggerCfg(ResourceLink rlink)
    {
        var holder = swconfIO.deserialize(LoggerCfg.class, rlink);

        if (holder != null)
        {
            var oldHolderConfig = holder.getConfig();
            var newHolderConfig = new LinkedHashMap<String, LoggingCfgEntry>();
            var parentRlink = holder.getParent();

            if (!StringUtils.isBlank(parentRlink))
            {
                if (parentRlink.equals("<default>"))
                    parentRlink = fallbackConfigLocation;

                var parent = readLoggerCfg(rlUtils.createLink(parentRlink));

                if (parent != null)
                {
                    for (var parentEntry : parent.getConfig().entrySet())
                    {
                        var key = parentEntry.getKey();
                        var value = parentEntry.getValue();

                        if (!oldHolderConfig.containsKey(key) && !newHolderConfig.containsKey(key))
                        {
                            newHolderConfig.put(key, value);
                        }
                    }
                }
            }

            newHolderConfig.putAll(oldHolderConfig);
            holder.setConfig(newHolderConfig);
        }

        return holder;
    }
}
