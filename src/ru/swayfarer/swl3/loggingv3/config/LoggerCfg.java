package ru.swayfarer.swl3.loggingv3.config;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.io.BinaryUtils;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.observable.property.DateTimeUtils;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl3.swconf2.mapper.annotations.EnableEnvs;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IgnoreSwconf;

@EnableEnvs(prefix = "loggingv3")
@Getter
@Setter
@Accessors(chain = true)
public class LoggerCfg {
    
    @IgnoreSwconf
    public transient BinaryUtils binaryUtils = BinaryUtils.getInstance();
    
    @IgnoreSwconf
    public transient DateTimeUtils dateTimeUtils = DateTimeUtils.getInstance();
    
    @IgnoreSwconf
    public transient IdentityHashMap<LoggingCfgEntry, IFunction1<StackTraceElement, Boolean>> cachedFilters = new IdentityHashMap<>();
    
    @IgnoreSwconf
    public transient Map<ExtendedList<Boolean>, LoggingCfgEntry> cachedEntries = new HashMap<>();

    @CommentedSwconf
    public LinkedHashMap<String, LoggingCfgEntry> config = new LinkedHashMap<>();

    @CommentedSwconf
    public String parent;

    public LoggerV3 apply(LoggerV3 logger)
    {
        if (logger == null)
        {
            return null;
        }
        
        var entry = findOrCreateEntry(logger);

        if (entry != null)
        {
            entry.apply(logger, binaryUtils, dateTimeUtils);
        }
        
        return logger;
    }

    public LoggingCfgEntry findOrCreateEntry(
            @NonNull LoggerV3 logger
    )
    {
        var chain = getFilterResult(logger);
        var ret = cachedEntries.get(chain);
        
        if (ret == null)
        {
            ret = constructConfigEntry(chain);
            cachedEntries.put(chain, ret);
        }
        
        return ret;
    }

    public boolean isAllFalse(ExtendedList<Boolean> filterResult)
    {
        return CollectionsSWL.isNullOrEmpty(filterResult) || !filterResult.contains(true);
    }

    public LoggingCfgEntry constructConfigEntry(@NonNull ExtendedList<Boolean> resultList)
    {
        return constructConfigEntry(resultList, new ExtendedList<>());
    }

    public LoggingCfgEntry constructConfigEntry(
            @NonNull ExtendedList<Boolean> resultList,
            @NonNull ExtendedList<LoggingCfgEntry> entriesChain
    )
    {
        var resultIterator = resultList.iterator();
        var resultEntry = new LoggingCfgEntry();
        
        for (var entry : config.entrySet())
        {
            if (Boolean.TRUE.equals(resultIterator.next()))
            {
                entriesChain.add(entry.getValue());
            }
        }
        
        entriesChain.sort();

        setLastNonNull(resultEntry, LoggingCfgEntry::getFilter, LoggingCfgEntry::setFilter, entriesChain);
        setLastNonNull(resultEntry, LoggingCfgEntry::getFormatting, LoggingCfgEntry::setFormatting, entriesChain);
        setLastNonNull(resultEntry, LoggingCfgEntry::getColoring, LoggingCfgEntry::setColoring, entriesChain);
        setLastNonNull(resultEntry, LoggingCfgEntry::getName, LoggingCfgEntry::setName, entriesChain);
        setLastNonNull(resultEntry, LoggingCfgEntry::getOutStreams, LoggingCfgEntry::setOutStreams, entriesChain);
        setLastNonNull(resultEntry, LoggingCfgEntry::getSkipCRChar, LoggingCfgEntry::setSkipCRChar, entriesChain);

        mergeCollections(resultEntry, LoggingCfgEntry::getFiles, LoggingCfgEntry::setFiles, entriesChain);
        mergeCollections(resultEntry, LoggingCfgEntry::getOutStreams, LoggingCfgEntry::setOutStreams, entriesChain);

        return resultEntry;
    }

    private <T> void mergeCollections(
            LoggingCfgEntry resultEntry,
            IFunction1<LoggingCfgEntry, ExtendedList<T>> getter,
            IFunction2<LoggingCfgEntry, ExtendedList<T>, LoggingCfgEntry> setter,
            ExtendedList<LoggingCfgEntry> entriesChain
    )
    {
        for (var entry : entriesChain)
        {
            var entryFiles = getter.apply(entry);
            var resultEntryFiles = getter.apply(resultEntry);

            if (!CollectionsSWL.isNullOrEmpty(entryFiles))
            {
                if (resultEntryFiles == null)
                {
                    resultEntryFiles = new ExtendedList<>();
                    setter.apply(resultEntry, resultEntryFiles);
                }

                for (var entryFile : entryFiles)
                {
                    resultEntryFiles.addExclusive(entryFile);
                }
            }
        }
    }

    public <T> void setLastNonNull(
            LoggingCfgEntry target,
            IFunction1<LoggingCfgEntry, T> getter,
            IFunction2<LoggingCfgEntry, T, LoggingCfgEntry> setter,
            ExtendedList<LoggingCfgEntry> entries
    )
    {
        var lastNonNullEntry = findLastNonNull(ReflectionsUtils.cast(getter), entries);

        if (lastNonNullEntry != null)
            setter.apply(target, getter.apply(lastNonNullEntry));
    }
    
    public LoggingCfgEntry findLastNonNull(@NonNull IFunction1<LoggingCfgEntry, Object> fun, @NonNull ExtendedList<LoggingCfgEntry> entries)
    {
        return findFirstNonNull(fun, entries.copy().reverse());
    }
    
    public LoggingCfgEntry findFirstNonNull(@NonNull IFunction1<LoggingCfgEntry, Object> fun, @NonNull ExtendedList<LoggingCfgEntry> entries)
    {
        return entries.exStream().findFirst((e) -> fun.apply(e) != null).orNull();
    }

    public ExtendedList<Boolean> getFilterResult(@NonNull LoggerV3 logger)
    {
        var filterResultChain = new ExtendedList<Boolean>();
        
        for (var cfgEntry : config.entrySet())
        {
            var entryName = cfgEntry.getKey();
            var entryValue = cfgEntry.getValue();
            
            var filter = getFilter(entryName, entryValue);
            var result = filter.apply(logger.getSource());
            
            filterResultChain.add(result);
        }
        
        return filterResultChain;
    }
    
    public IFunction1<StackTraceElement, Boolean> getFilter(@NonNull String name, @NonNull LoggingCfgEntry cfgEntry)
    {
        var ret = cachedFilters.get(cfgEntry);
        
        if (ret == null)
        {
            var accepts = cfgEntry.getAccepts();
            
            if (accepts == null)
            {
                ret = (e) -> e.getClassName().startsWith(name);
            }
            else
            {
                ret = accepts.asFilterFun();
            }
            
            cachedFilters.put(cfgEntry, ret);
        }
        
        return ret;
    }
}
