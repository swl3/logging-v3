package ru.swayfarer.swl3.loggingv3.config;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.io.BinaryUtils;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.observable.property.DateTimeUtils;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl3.system.SystemUtils;

@Getter
@Setter
@Accessors(chain = true)
public class LoggingCfgEntry implements Comparable<LoggingCfgEntry> {
    
    @CommentedSwconf
    public String name;
    
    @CommentedSwconf
    public int priority;
    
    @CommentedSwconf
    public SourceFiltering accepts;
    
    @CommentedSwconf
    public FilteringCfg filter;
    
    @CommentedSwconf
    public FormattingCfg formatting;
    
    @CommentedSwconf
    public ColoringCfg coloring;
    
    @CommentedSwconf
    public ExtendedList<FileCfg> files;

    @CommentedSwconf
    public Boolean skipCRChar = true;
    
    public ExtendedList<OutStreamCfg> outStreams;

    public LoggerV3 apply(LoggerV3 logger, @NonNull BinaryUtils binaryUtils, @NonNull DateTimeUtils timeUtils)
    {
        if (logger == null)
        {
            return null;
        }

        if (Boolean.TRUE.equals(skipCRChar))
        {
            var isMac = SystemUtils.getInstance().isMac();

            logger.eventFiltering.subscribe().by((e) -> {
                var content = e.getContent();
                content.contentStr = content.contentStr.andApply((s) -> {
                    if (s != null && !isMac)
                        return s.replace("\r", "");
                    return s;
                }).memorized();
            });
        }

        if (filter != null)
        {
            filter.apply(logger);
        }
        
        if (formatting != null)
        {
            formatting.apply(logger);
        }
        
        if (coloring != null)
        {
            coloring.apply(logger);
        }
        
        if (!CollectionsSWL.isNullOrEmpty(files))
        {
            for (var file : files)
            {
                file.apply(timeUtils, binaryUtils, logger);
            }
        }

        if (!CollectionsSWL.isNullOrEmpty(outStreams))
        {
            for (var out : outStreams)
            {
                out.apply(logger, timeUtils);
            }
        }
        
        return logger;
    }

    @Override
    public int compareTo(LoggingCfgEntry o)
    {
        if (o == null)
            return 1;
        
        return priority - o.priority;
    }
}
