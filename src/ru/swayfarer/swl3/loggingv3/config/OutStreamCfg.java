package ru.swayfarer.swl3.loggingv3.config;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import static ru.swayfarer.swl3.collections.CollectionsSWL.list;

import java.io.OutputStream;
import java.util.concurrent.TimeUnit;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.loggingv3.event.io.OutStreamLogHandler;
import ru.swayfarer.swl3.observable.property.DateTimeUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;
import ru.swayfarer.swl3.swconf2.mapper.annotations.IgnoreSwconf;

@Getter
@Setter
@Accessors(chain = true)
public class OutStreamCfg {

    @IgnoreSwconf
    public ExtendedMap<ExtendedList<String>, IFunction0<OutputStream>> namesToStreamFun = CollectionsSWL.exMap
    (
            list("o", "out", "output", "sout", "sysout"), systemOut(),
            list("e", "err", "error", "serr", "syserr"), systemErr()
    )
    .synchronize();
    
    @CommentedSwconf
    public String target;
    
    @CommentedSwconf
    public String flushPer;
    
    @CommentedSwconf
    public FilteringCfg fitler;

    public OutStreamCfg()
    {
        ;
    }
    
    public LoggerV3 apply(@NonNull LoggerV3 logger, @NonNull DateTimeUtils timeUtils)
    {
        if (!StringUtils.isBlank(target))
        {
            OutputStream os = findOutStream(target);

            if (os != null)
            {
                var osHandler = new OutStreamLogHandler(DataOutStream.of(os));
                var filteredHandler = FilteringCfg.filtered(
                        fitler, 
                        osHandler
                );
                
                if (!StringUtils.isBlank(flushPer))
                {
                    var delay = timeUtils.milisecondsOf(flushPer).longValue();
                    osHandler.flushPer(delay, TimeUnit.MILLISECONDS);
                }
                
                logger.eventProcess.subscribe().by(filteredHandler);
            }
        }
        
        return logger;
    }
    
    public OutputStream findOutStream(@NonNull String name)
    {
        var entry = namesToStreamFun.entrySet()
                .stream()
                    .filter((e) -> e.getKey().contains(name))
                    .findFirst()
                    .orElse(null)
        ;
        
        if (entry == null)
        {
            return null;
        }
        
        var fun = entry.getValue();
        
        if (fun == null)
        {
            return null;
        }
        
        return fun.apply();
    }
    
    public static IFunction0<OutputStream> systemOut()
    {
        return () -> System.out;
    }
    
    public static IFunction0<OutputStream> systemErr()
    {
        return () -> System.err;
    }
}
