package ru.swayfarer.swl3.loggingv3.config;

import lombok.var;
import lombok.Data;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.loggingv3.filtering.StandartStacktraceFilters;
import ru.swayfarer.swl3.swconf2.mapper.annotations.CommentedSwconf;

@Data
@Accessors(chain = true)
public class SourceFiltering {
    
    @CommentedSwconf
    public ExtendedList<String> pkg = new ExtendedList<>();
    
    @CommentedSwconf
    public ExtendedList<String> containers = new ExtendedList<>();
    
    public IFunction1<StackTraceElement, Boolean> asFilterFun()
    {
        IFunction1<StackTraceElement, Boolean> ret = (st) -> true;
        
        if (pkg != null)
        {
            var pkgFilter = StandartStacktraceFilters.classExpressions(pkg);
            ret = ret.and(pkgFilter);
        }
        
        if (containers != null)
        {
            var containerFilter = StandartStacktraceFilters.sourceExpressions(containers);
            ret = ret.and(containerFilter);
        }
        
        return ret;
    }
}
