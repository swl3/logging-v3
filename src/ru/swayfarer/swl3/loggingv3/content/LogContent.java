package ru.swayfarer.swl3.loggingv3.content;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.LazyThrowable;
import ru.swayfarer.swl3.funs.GeneratedFuns;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.loggingv3.event.LogThrowableInfo;
import ru.swayfarer.swl3.loggingv3.event.LogThrowables;

@Getter
@Setter
@Accessors(chain = true)
public class LogContent {
    
    @NonNull
    public IFunction0<String> contentStr;
    
    public LogThrowables throwables;

    public LogContent(@NonNull IFunction0<String> contentStr)
    {
        this.contentStr = contentStr;
    }

    public boolean hasThrowable()
    {
        return throwables != null;
    }
    
    public LogContent setThrowable(Throwable e)
    {
        if (e == null)
        {
            throwables = null;
            return this;
        }

        throwables = new LogThrowables(new LazyThrowable<>(e));
        
        return this;
    }

    public String getContentStr()
    {
        return contentStr.apply();
    }

    public LogContent setContentStr(@NonNull IFunction0<String> contentStr)
    {
        this.contentStr = contentStr;
        return this;
    }

    public LogContent setContentStr(String contentStr)
    {
        return setContentStr(() -> contentStr);
    }

    public LogContent setContentStr(Object contentStr)
    {
        return setContentStr(String.valueOf(contentStr));
    }
}
