package ru.swayfarer.swl3.loggingv3.content;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor @NoArgsConstructor
public class ThrowableInfo {
    
    @NonNull
    public Throwable throwable;
    
    public IFunction0<ExtendedList<StackTraceElement>> stacktraceFun;
    
    public ExtendedList<StackTraceElement> getStacktrace()
    {
        return stacktraceFun.apply();
    }
    
    {
        stacktraceFun = ThreadsUtils.singleton(() -> CollectionsSWL.list(throwable.getStackTrace()));
    }
}
