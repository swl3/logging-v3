package ru.swayfarer.swl3.loggingv3.event;

import lombok.var;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import ru.swayfarer.swl3.loggingv3.LoggerV3;
import ru.swayfarer.swl3.loggingv3.content.LogContent;
import ru.swayfarer.swl3.loggingv3.level.ILogLevel;
import ru.swayfarer.swl3.observable.event.AbstractCancelableEvent;
import ru.swayfarer.swl3.observable.event.EventSource;

@Data @EqualsAndHashCode(callSuper = true)
@Accessors
@SuperBuilder
public class LogEvent extends AbstractCancelableEvent {
    
    @NonNull
    public Thread thread;
    
    @NonNull
    public LoggerV3 logger;
    
    @NonNull
    public LogContent content;
    
    @NonNull
    public ILogLevel level;
    
    public EventSource source;
    
    public LogEvent updateSource(int offset)
    {
        this.source = new EventSource(2 + offset);
        return this;
    }
}
