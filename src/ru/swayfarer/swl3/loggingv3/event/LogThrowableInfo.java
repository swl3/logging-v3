package ru.swayfarer.swl3.loggingv3.event;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.LazyThrowable;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Data
@Accessors(chain = true)
public class LogThrowableInfo {

    public IFunction0<ExtendedList<Entry>> logEntries = ThreadsUtils.singleton(
            () -> getThrowable()
                    .getStacktrace()
                    .map(this::entryOf)
    );
    
    @NonNull
    public LazyThrowable<Throwable> throwable;

    @NonNull
    public IFunction0<String> header = ThreadsUtils.singleton(() -> throwable.getThrowable().toString());
    
    public Entry entryOf(@NonNull StackTraceElement stackTraceElement)
    {
        return new Entry()
                .setText(stackTraceElement.toString())
                .setStackTraceElement(stackTraceElement)
        ;
    }
    
    public ExtendedList<Entry> getLogEntries()
    {
        return logEntries.apply();
    }

    public String getHeader()
    {
        return header.apply();
    }

    public LogThrowableInfo setHeader(@NonNull String header)
    {
        this.header = () -> header;
        return this;
    }
    
    @Data
    @Accessors(chain = true)
    public static class Entry {
        public StackTraceElement stackTraceElement;
        public String text;
    }
}
