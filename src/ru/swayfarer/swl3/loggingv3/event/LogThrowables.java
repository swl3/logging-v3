package ru.swayfarer.swl3.loggingv3.event;

import lombok.var;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.exception.LazyThrowable;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class LogThrowables
{
    public LazyThrowable<Throwable> throwable;

    public LogThrowables(LazyThrowable<Throwable> throwable)
    {
        this.throwable = throwable;
    }

    public IFunction0<ExtendedList<LogThrowableInfo>> entries = ThreadsUtils.singleton(() -> {
        var ret = new ExtendedList<LazyThrowable<Throwable>>();

        var lazyThrowable = getThrowable();
        var e = lazyThrowable.getThrowable();

        while (e != null)
        {
            lazyThrowable = new LazyThrowable<>(e);
            ret.add(lazyThrowable);
            e = e.getCause();
        }

        return ret.map(LogThrowableInfo::new);
    });

    public ExtendedList<LogThrowableInfo> getEntries()
    {
        return entries.apply();
    }
}
