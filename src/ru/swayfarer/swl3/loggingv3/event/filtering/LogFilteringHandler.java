package ru.swayfarer.swl3.loggingv3.event.filtering;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;

@Getter
@Setter
@Accessors(chain = true)
public class LogFilteringHandler implements ILogHandler{

    @NonNull
    public FunctionsChain1<LogEvent, Boolean> filterFuns = new FunctionsChain1<>();
    
    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        var isFiltered = filterFuns.apply(event);

        if (Boolean.FALSE.equals(isFiltered))
        {
            event.setCanceled(true);
            return;
        }
    }
    
    public LogFilteringHandler rule(@NonNull IFunction1<LogEvent, Boolean> filterFun)
    {
        filterFuns.add(filterFun);
        return this;
    }
    
    public LogFilteringHandler rule(@NonNull IFunction1<LogEvent, Boolean> filterFun, int priority)
    {
        filterFuns.add(filterFun, priority);
        return this;
    }
}
