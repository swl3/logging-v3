package ru.swayfarer.swl3.loggingv3.event.filtering;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;

@Data
@Accessors(chain = true)
public class StacktraceFilteringHandler implements ILogHandler{

    public FunctionsChain1<StackTraceElement, Boolean> filteringFuns = new FunctionsChain1<>();
    
    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        var throwables = event.getContent().getThrowables();

        for (var throwable : throwables.getEntries())
        {
            throwable.getLogEntries().filter((e) -> accept(e.getStackTraceElement()));
        }
    }
    
    public boolean accept(StackTraceElement event)
    {
        return Boolean.TRUE.equals(filteringFuns.apply(event));
    }

}
