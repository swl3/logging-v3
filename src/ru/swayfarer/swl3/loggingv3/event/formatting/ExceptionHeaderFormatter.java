package ru.swayfarer.swl3.loggingv3.event.formatting;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.loggingv3.formatting.StandartFormatting;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;
import ru.swayfarer.swl3.string.insertions.InsertionsReplaceEvent;
import ru.swayfarer.swl3.string.insertions.replacer.NoDeepInsertionsReplacer;

public class ExceptionHeaderFormatter implements ILogHandler, IHasInsertionRules
{
    @NonNull
    public String headerFormat;

    @NonNull
    public NoDeepInsertionsReplacer insertionsReplacer = new NoDeepInsertionsReplacer();

    public ExceptionHeaderFormatter(@NonNull String headerFormat)
    {
        this.headerFormat = headerFormat;
    }

    @Override
    public void addInsertionRule(EventInserionRule event)
    {
        insertionsReplacer.getInsertionFuns().add(event);
    }

    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        var logThrowables = event.getContent().getThrowables();

        if (logThrowables != null)
            for (var logThrowable : logThrowables.getEntries())
            {
                if (logThrowable != null)
                {
                    var headerInsertionEvent = InsertionsReplaceEvent.builder().replaceString(headerFormat).build();
                    StandartFormatting.setLogEvent(headerInsertionEvent, event);
                    setThrowable(headerInsertionEvent, logThrowable.getThrowable().getThrowable());

                    logThrowable.setHeader(insertionsReplacer.replace(headerInsertionEvent));
                }
            }
    }

    public static <T extends AbstractEvent> T setThrowable(@NonNull T event, @NonNull Throwable throwable)
    {
        var customAttributes = event.getCustomAttributes();
        customAttributes.put("targetLogThrowable", throwable);
        return event;
    }

    public static Throwable getThrowable(@NonNull AbstractEvent event)
    {
        var customAttributes = event.getCustomAttributes();
        return customAttributes.getValue("targetLogThrowable");
    }

    public FunctionsChain1<InsertionEvent, String> getInsertionFuns()
    {
        return insertionsReplacer.getInsertionFuns();
    }
}
