package ru.swayfarer.swl3.loggingv3.event.formatting;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.loggingv3.formatting.StandartFormatting;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;
import ru.swayfarer.swl3.string.insertions.InsertionsReplaceEvent;
import ru.swayfarer.swl3.string.insertions.replacer.NoDeepInsertionsReplacer;

@Getter
@Setter
@Accessors(chain = true)
public class ExceptionStacktraceFormatter implements ILogHandler, IHasInsertionRules{

    @NonNull
    public String elementFormat;
    
    @NonNull
    public NoDeepInsertionsReplacer insertionsReplacer = new NoDeepInsertionsReplacer();

    public ExceptionStacktraceFormatter(@NonNull String elementFormat)
    {
        this.elementFormat = elementFormat;
    }

    @Override
    public void handle(@NonNull LogEvent logEvent) throws Throwable
    {
        var logThrowables = logEvent.getContent().getThrowables();

        if (logThrowables != null)
        for (var logThrowable : logThrowables.getEntries())
        {
            if (logThrowable != null)
            {
                var logEntries = logThrowable.getLogEntries();

                for (var logEntry : logEntries)
                {
                    var insertionEvent = InsertionsReplaceEvent.builder()
                            .replaceString(elementFormat)
                            .build()
                    ;

                    StandartFormatting.setLogEvent(insertionEvent, logEvent);
                    setStacktraceElement(insertionEvent, logEntry.getStackTraceElement());

                    logEntry.setText(insertionsReplacer.replace(insertionEvent));
                }
            }
        }
    }
    
    public FunctionsChain1<InsertionEvent, String> getInsertionFuns()
    {
        return insertionsReplacer.getInsertionFuns();
    }
    
    public static <T extends AbstractEvent> T setStacktraceElement(@NonNull T event, @NonNull StackTraceElement element)
    {
        var customAttributes = event.getCustomAttributes();
        customAttributes.put("targetStacktraceElement", element);
        return event;
    }
    
    public static StackTraceElement getStacktraceElement(@NonNull AbstractEvent event)
    {
        var customAttributes = event.getCustomAttributes();
        return customAttributes.getValue("targetStacktraceElement");
    }

    @Override
    public void addInsertionRule(EventInserionRule event)
    {
        getInsertionFuns().add(event);
    }
}
