package ru.swayfarer.swl3.loggingv3.event.formatting;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.var;
import java.util.Map;

import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.Singular;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.loggingv3.formatting.insertion.InsertionsLogFormattingRule;
import ru.swayfarer.swl3.loggingv3.info.LogFormattingInfo;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;

@Getter
@Setter
@Accessors(chain = true)
public class FormattingLogHandler implements ILogHandler, IHasInsertionRules {

    public IObservable<LogFormattingEvent> eventFormatting = Observables.createObservable();
    
    public LogFormattingInfo formattingInfo;

    public FormattingLogHandler(@NonNull LogFormattingInfo formattingInfo)
    {
        this.formattingInfo = formattingInfo;
    }

    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        var formattingEvent = LogFormattingEvent.builder()
            .formattingLogHandler(this)
            .logEvent(event)
            .formattingInfo(formattingInfo)
            .build()
        ;
        
        eventFormatting.next(formattingEvent);
        
        event.getContent().setContentStr(formattingEvent.getResultStr());
    }

    @Override
    public void addInsertionRule(EventInserionRule event)
    {
        var insertionsRuleFormatters = eventFormatting.tasks()
                .findTaskObjects(InsertionsLogFormattingRule.class)
        ;

        for (var insFormatter : insertionsRuleFormatters)
        {
            insFormatter.insertionsReplacer.getInsertionFuns().add(event);
        }
    }

    @Getter
    @Setter
    @SuperBuilder
    @Accessors(chain = true)
    public static class LogFormattingEvent {
        
        public String resultStr;
        
        @NonNull
        public LogFormattingInfo formattingInfo;
        
        @NonNull
        public LogEvent logEvent;
        
        @NonNull
        public FormattingLogHandler formattingLogHandler;

        @Singular
        public Map<String, Object> additionalProperties;
    }
    
    public static interface ILogFormatRule extends IFunction1NoR<LogFormattingEvent> {}
}
