package ru.swayfarer.swl3.loggingv3.event.formatting;

import lombok.var;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;

public interface IHasInsertionRules
{
    public void addInsertionRule(EventInserionRule event);
}
