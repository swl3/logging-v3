package ru.swayfarer.swl3.loggingv3.event.formatting;

import lombok.var;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.loggingv3.coloring.ColoringManager;
import ru.swayfarer.swl3.loggingv3.content.LogContent;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.LogThrowableInfo;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
public class LogLevelColorFormatter implements ILogHandler
{
    public ExtendedMap<String, String> levelToColorMap = new ExtendedMap<>().synchronize();

    @NonNull
    public ColoringManager coloringManager;

    @NonNull
    public String palette;

    public LogLevelColorFormatter(@NonNull ColoringManager coloringManager, @NonNull String palette)
    {
        this.coloringManager = coloringManager;
        this.palette = palette;
    }

    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        var levelName = event.getLevel().getName();
        var levelColorText = coloringManager.getColorPrefix(palette, "fg_" + StringUtils.orEmpty(levelToColorMap.getValue(levelName)));

        var logContent = event.getContent();

        var keys = new String[] { "color", "lc", "levelColor", "levelCol", "lcol" };

        replace(LogContent::getContentStr, LogContent::setContentStr, logContent, levelColorText, keys);

        if (logContent.hasThrowable())
        {
            for (var throwable : logContent.getThrowables().getEntries())
            {
                replace(LogThrowableInfo::getHeader, LogThrowableInfo::setHeader, throwable, levelColorText, keys);

                for (var entry : throwable.getLogEntries())
                {
                    replace(LogThrowableInfo.Entry::getText, LogThrowableInfo.Entry::setText, entry, levelColorText, keys);
                }
            }
        }
    }

    public <T> void replace(IFunction1<T, String> getter, IFunction2<T, String, T> setter, T source, String newValue, String... keys)
    {
        var oldStr = getter.apply(source);
        var newStr = replace(newValue, oldStr, keys);
        setter.apply(source, newStr);
    }

    public String replace(String value, String source, String... keys)
    {
        for (var key : keys)
        {
            var str = "$" + key;
            source = source.replace(str, value);
        }

        return source;
    }
}
