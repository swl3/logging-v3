package ru.swayfarer.swl3.loggingv3.event.handlers;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;

public interface ILogHandler extends IFunction1NoR<LogEvent> {
    
    @Override
    default void applyNoRUnsafe(LogEvent event) throws Throwable
    {
        handle(event);
    }
    
    public void handle(@NonNull LogEvent event) throws Throwable;
}
