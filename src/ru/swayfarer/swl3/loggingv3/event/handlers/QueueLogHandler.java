package ru.swayfarer.swl3.loggingv3.event.handlers;

import lombok.var;
import java.util.concurrent.Executor;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.observable.executor.ObservableExcecutors;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class QueueLogHandler implements ILogHandler {

    @NonNull
    public IObservable<LogEvent> eventLog = Observables.createObservable();
    
    @NonNull
    public Executor executor;
    
    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        eventLog.next(event);
    }

    public QueueLogHandler setThreadsCount(int count)
    {
        ExceptionsUtils.IfNot(
                count > 0, 
                IllegalArgumentException.class, 
                "Error while setting threads count to '" + count + "'. Value must be bigger than 0!"
        );
        
        var executor = ThreadsUtils.threadsExecutor()
                .startThreadsCount(0)
                .maxThreadsCount(count)
                .keepAliveTime(3)
                .get()
        ;
        
        return setExecutor(executor);
    }
    
    public QueueLogHandler setExecutor(@NonNull Executor executor)
    {
        this.executor = executor;
        this.eventLog.execution().executor(ObservableExcecutors.forObservable(executor));
        return this;
    }
    
    {
        setThreadsCount(1);
    }
}
