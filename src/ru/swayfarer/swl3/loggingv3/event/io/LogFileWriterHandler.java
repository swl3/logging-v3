package ru.swayfarer.swl3.loggingv3.event.io;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.util.concurrent.atomic.AtomicBoolean;

import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.string.StringUtils;

@Getter
@Setter
@Accessors(chain = true)
public class LogFileWriterHandler implements ILogHandler {

    @NonNull
    public OutStreamLogHandler outWriter;
    
    @NonNull
    public IObservable<LogFileWriterHandler> eventArchive = Observables.createObservable();
    
    @NonNull
    public FunctionsChain1<LogFileWriterHandler, Boolean> archiveConditionsFuns = new FunctionsChain1<>();
    
    @NonNull
    public IFunction0<FileSWL> logFileFun;
    
    public FileSWL logFile;
    
    @NonNull
    public AtomicBoolean isDirty = new AtomicBoolean();
    
    @NonNull
    public ExtendedMap<String, Object> customAttributes = new ExtendedMap<>().synchronize();

    public String ansiColorsRegex = StringUtils.regex()
        .text("\033[")
        .some().not("m")
        .text("m")
        .build()
    ;
    
    public LogFileWriterHandler(@NonNull String fileNamePattern)
    {
        this(() -> FileSWL.of(fileNamePattern));
    }
    
    public LogFileWriterHandler(@NonNull IFunction0<FileSWL> logFile)
    {
        this.logFileFun = logFile;
        archiveConditionsFuns.setFilterFun(Boolean.TRUE::equals);
    }

    public synchronized LogFileWriterHandler updateFileIfNeeded()
    {
        if (this.logFile == null || this.outWriter == null)
        {
            updateFile();
        }

        return this;
    }
    
    public synchronized LogFileWriterHandler updateFile()
    {
        this.logFile = logFileFun.apply();
        logFile.createIfNotFound();
        outWriter = new OutStreamLogHandler(this.logFile.append());
        
        outWriter.appendPreprocessor((s) -> s.replaceAll(ansiColorsRegex, ""));
        
        return this;
    }
    
    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        updateFileIfNeeded();
        isDirty.set(true);
        outWriter.handle(event);
        
        archiveIfNeed(false);
    }
    
    public void archiveIfNeed(boolean skipConditions)
    {
        if (isNeedsToArchive(skipConditions))
        {
            eventArchive.next(this);
            isDirty.set(false);
            updateFile();
        }
    }
    
    public boolean isNeedsToArchive(boolean skipConditions)
    {
        return isDirty.get() && (skipConditions || Boolean.TRUE.equals(archiveConditionsFuns.apply(this)));
    }
    
    public LogFileWriterHandler attribute(@NonNull String name, @NonNull Object value)
    {
        this.customAttributes.put(name, value);
        return this;
    }
}
