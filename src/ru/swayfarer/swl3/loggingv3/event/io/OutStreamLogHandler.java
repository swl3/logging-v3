package ru.swayfarer.swl3.loggingv3.event.io;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.io.streams.DataOutStream;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.thread.ThreadsUtils;
import ru.swayfarer.swl3.threads.lock.NotifyLock;

@Getter
@Setter
@Accessors(chain = true)
public class OutStreamLogHandler implements ILogHandler{
    
    @NonNull
    public IFunction0NoR flushFun = () -> {
        var writer = getWriter();
        writer.flush();
    };
    
    @NonNull
    public IObservable<OutStreamLogHandler> eventPostWrite = Observables.createObservable();
    
    @NonNull
    public DataOutStream dos;
    
    public IFunction0<BufferedWriter> writer = ThreadsUtils.threadLocal(() -> new BufferedWriter(new OutputStreamWriter(getDos()), 1024 * 32));
    
    public IFunction1<String, String> contentPreprocessor = (s) -> s;

    public OutStreamLogHandler(@NonNull DataOutStream dos)
    {
        this.dos = dos;
    }
    
    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        var contentStr = event.getContent().getContentStr();
        contentStr = applyPreprocessing(contentStr);
        var writer = getWriter();

        writer.append(contentStr);

        var prefix = "";
        var throwables = event.getContent().getThrowables();

        if (throwables != null)
        for (var throwable : throwables.getEntries())
        {
            writer.newLine();
            writer.append(prefix);
            writer.append(applyPreprocessing(throwable.getHeader()));

            for (var entry : throwable.getLogEntries())
            {
                writer.newLine();
                writer.append(applyPreprocessing(entry.getText()));
            }

            prefix = "Caused by: ";
        }

        eventPostWrite.next(this);

        writer.newLine();
        flushFun.apply();
    }
    
    public OutStreamLogHandler appendPreprocessor(@NonNull IFunction1<String, String> fun)
    {
        this.contentPreprocessor = this.contentPreprocessor.andAfter(fun);
        return this;
    }

    public String applyPreprocessing(Object o)
    {
        if (o == null)
            return null;

        var str = o instanceof String ? (String) o : String.valueOf(o);

        if (contentPreprocessor != null)
        {
            return contentPreprocessor.apply(str);
        }

        return str;
    }
    
    public OutStreamLogHandler flushPer(long delay, @NonNull TimeUnit timeUnit)
    {
        var delayInMilisis = timeUnit.toMillis(delay);
        var thread = new FlusherThread(this);
        thread.getDelay().set(delayInMilisis);
        thread.start();
        
        Runtime.getRuntime().addShutdownHook(new Thread(() -> ExceptionsUtils.safe(getWriter()::flush)));
        
        flushFun = thread::notifyThread;
        
        return this;
    }

    @Getter
    @Setter
    @Accessors(chain = true)
    public static class FlusherThread extends Thread {
        
        public volatile NotifyLock flusherThreadLock = new NotifyLock();
        public AtomicLong delay = new AtomicLong(500);
        
        @NonNull
        public OutStreamLogHandler outWriter;

        public FlusherThread(OutStreamLogHandler outWriter)
        {
            this.outWriter = outWriter;
        }
        
        @Override
        public void run()
        {
            for (;;)
            {
                try {
                    waitForUnlock();
                    outWriter.getWriter().flush();
                    Thread.sleep(delay.get());
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        
        public void waitForUnlock()
        {
            flusherThreadLock.waitFor();
        }
        
        public void notifyThread()
        {
            flusherThreadLock.notifyLockAll();
        }
        
        {
            setDaemon(true);
        }
    }

    public BufferedWriter getWriter()
    {
        return writer.apply();
    }
}
