package ru.swayfarer.swl3.loggingv3.exception;

@SuppressWarnings("serial")
public class LoggingInitializationException extends RuntimeException{

    public LoggingInitializationException()
    {
        super();
    }

    public LoggingInitializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public LoggingInitializationException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public LoggingInitializationException(String message)
    {
        super(message);
    }

    public LoggingInitializationException(Throwable cause)
    {
        super(cause);
    }
}
