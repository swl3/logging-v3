package ru.swayfarer.swl3.loggingv3.filtering;

import lombok.var;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.handlers.ILogHandler;
import ru.swayfarer.swl3.string.FilteringList;

@Getter
@Setter
@Accessors(chain = true)
public class LogStacktraceFilter implements ILogHandler
{
    public FilteringList filters = new FilteringList();

    @Override
    public void handle(@NonNull LogEvent event) throws Throwable
    {
        var content = event.getContent();
        var throwables = content.getThrowables();

        if (throwables != null)
        {
            for (var throwable : throwables.getEntries())
            {
                var logEntries = throwable.getLogEntries();
                logEntries.removeIf((logEntry) -> !filters.isMatches(getStringFromStacktrace(logEntry.getStackTraceElement())));
            }
        }
    }

    public String getStringFromStacktrace(StackTraceElement stackTraceElement)
    {
        return stackTraceElement.getClassName().concat(stackTraceElement.getMethodName());
    }
}
