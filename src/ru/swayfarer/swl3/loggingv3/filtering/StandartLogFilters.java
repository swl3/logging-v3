package ru.swayfarer.swl3.loggingv3.filtering;

import lombok.var;
import java.util.List;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.string.FilteringList;

public class StandartLogFilters {
    
    public static IFunction1<LogEvent, Boolean> maxLevel(int level)
    {
        return (e) -> e.getLevel().getWeight() <= level;
    }
    
    public static IFunction1<LogEvent, Boolean> minLevel(int level)
    {
        return (e) -> e.getLevel().getWeight() >= level;
    }
    
    public static IFunction1<LogEvent, Boolean> levelExpressions(@NonNull List<String> content)
    {
        return expressions(content, (e) -> e.getLevel().getName());
    }
    
    public static IFunction1<LogEvent, Boolean> contentExpressions(@NonNull List<String> content)
    {
        return expressions(content, (e) -> e.getContent().getContentStr());
    }
    
    public static IFunction1<LogEvent, Boolean> expressions(@NonNull List<String> content, @NonNull IFunction1<LogEvent, String> expressionFun)
    {
        var filtering = new FilteringList();
        filtering.addAll(content);
        
        return (e) -> {
            var contentStr = expressionFun.apply(e);
            return filtering.isMatches(contentStr);
        };
    }
    
    public static IFunction1<LogEvent, Boolean> bySource(@NonNull IFunction1<StackTraceElement, Boolean> fun)
    {
        return (evt) -> fun.apply(evt.getSource().getSource());
    }
}
