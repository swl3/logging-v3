package ru.swayfarer.swl3.loggingv3.filtering;

import lombok.var;
import java.security.CodeSource;
import java.util.List;

import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.reflection.ClassUtil;
import ru.swayfarer.swl3.reflection.ClassLocationHelper;
import ru.swayfarer.swl3.string.FilteringList;

public class StandartStacktraceFilters {
    
    public static volatile CodeSource mainCodeSource;
    
    public static IFunction1<StackTraceElement, Boolean> classExpressions(@NonNull List<String> expressions)
    {
        return expressions(StackTraceElement::getClassName, expressions);
    }
    
    public static IFunction1<StackTraceElement, Boolean> notMainSource()
    {
        return mainSource().andApply((e) -> !e);
    }
    
    public static IFunction1<StackTraceElement, Boolean> mainSource()
    {
        return (st) -> {
            var cl = ClassUtil.forName(st.getClassName());
            return cl.getProtectionDomain().getCodeSource() == mainCodeSource;
        };
    }
    
    public static IFunction1<StackTraceElement, Boolean> sourceExpressions(@NonNull List<String> expressions)
    {
        return expressions((st) -> {
            var cl = ClassUtil.forName(st.getClassName());
            var loc = ClassLocationHelper.findClassLocation(cl);
            
            if (loc != null)
            {
                var containerSimpleName = loc.getContainerSimpleName();
                return containerSimpleName;
            }
            
            return null;
            
        }, expressions);
    }
    
    public static IFunction1<StackTraceElement, Boolean> expressions(@NonNull IFunction1<StackTraceElement, String> strFun, @NonNull List<String> expressions)
    {
        var filtering = new FilteringList();
        filtering.addAll(expressions);
        
        return (e) -> {
            var str = strFun.apply(e);
            return str == null ? true : filtering.isMatches(str);
        };
    }
}
