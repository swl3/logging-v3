package ru.swayfarer.swl3.loggingv3.formatting;

import lombok.var;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;

public class StandardExceptionsHeadersInsertions
{
    public static EventInserionRule localizedMessage()
    {
        return StandardInsertionsPresets.replaceByThrowable(
                (insEvent, elem) -> elem.getLocalizedMessage(),
                "lmsg", "lm", "ltext", "lmessage", "localized", "localizedMessage"
        );
    }

    public static EventInserionRule message()
    {
        return StandardInsertionsPresets.replaceByThrowable(
                (insEvent, elem) -> elem.getMessage(),
                "msg", "m", "text", "message"
        );
    }

    public static EventInserionRule className()
    {
        return StandardInsertionsPresets.replaceByThrowable(
                (insEvent, elem) -> StandartStacktraceInsertions.abbreviateClassName(insEvent.getInsertion(), elem.getClass().getName()),
                "class", "cl", "cls", "c"
        );
    }

    public static void addAllTo(FunctionsChain1<InsertionEvent, String> rules)
    {
        rules.add(localizedMessage());
        rules.add(message());
        rules.add(className());
    }
}
