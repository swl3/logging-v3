package ru.swayfarer.swl3.loggingv3.formatting;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction2NoR;
import ru.swayfarer.swl3.loggingv3.event.formatting.ExceptionHeaderFormatter;
import ru.swayfarer.swl3.loggingv3.event.formatting.FormattingLogHandler.LogFormattingEvent;
import ru.swayfarer.swl3.loggingv3.event.formatting.ExceptionStacktraceFormatter;
import ru.swayfarer.swl3.markers.Alias;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;
import ru.swayfarer.swl3.string.insertions.replacer.StandartInsertionRules;

public class StandardInsertionsPresets
{
    
    public static EventInserionRule replace(@NonNull IFunction1NoR<InsertionEvent.WithResult> strFun, @NonNull String... keys)
    {
        var ret = StandartStringFormatters.defaultStringRules();
        ret.eventInsert
                .subscribe().by(strFun::apply)
                .executeIf(StandartInsertionRules.filterByKey(keys));

        return ret;
    }

    public static EventInserionRule replaceInLog(@NonNull IFunction1<LogFormattingEvent, String> strFun, @NonNull String... keys)
    {
        return replaceInLog((formatEvt, insEvt) -> {
            var str = strFun.apply(formatEvt);
            insEvt.setResult(str);
        }, keys);
    }

    public static EventInserionRule replaceInLog(@NonNull IFunction2NoR<LogFormattingEvent, InsertionEvent.WithResult> strFun, @NonNull String... keys) 
    {
        return replace((event) -> {
            var formatEvent = StandartFormatting.getFormatEvent(event);
            if (formatEvent != null)
            {
                strFun.apply(formatEvent, event);
            }
        }, keys);
    }
    
    public static EventInserionRule replaceInStacktrace(@NonNull IFunction2<InsertionEvent, StackTraceElement, String> strFun, @NonNull String... keys)
    {
        return replace((event) -> {
            var stacktraceElement = ExceptionStacktraceFormatter.getStacktraceElement(event);
            if (stacktraceElement != null)
            {
                var apply = strFun.apply(event, stacktraceElement);
                event.setResult(apply);
            }
        }, keys);
    }

    public static EventInserionRule replaceByThrowable(@NonNull IFunction2<InsertionEvent, Throwable, String> strFun, @NonNull String... keys)
    {
        return replace((event) -> {
            var throwable = ExceptionHeaderFormatter.getThrowable(event);

            if (throwable != null)
            {
                var apply = strFun.apply(event, throwable);
                event.setResult(apply);
            }
        }, keys);
    }
    
    public static EventInserionRule replaceInStacktraceByPart(@NonNull IInsertionRulePart strFun, @NonNull String... keys)
    {
        var rule = strFun.asStandaloneRule();
        return replaceInStacktrace((insertionEvent, formattingEvent) -> {
            return rule.apply(insertionEvent);
        }, keys);
    }
    
    public static EventInserionRule replaceInStacktrace(@NonNull IFunction1<StackTraceElement, String> strFun, @NonNull String... keys)
    {
        return replaceInStacktrace((insEvent, stelem) -> strFun.apply(stelem), keys);
    }
    
    public static interface IInsertionRule extends IFunction1<InsertionEvent, String>{}
    
    public static interface IInsertionRulePart extends IFunction1NoR<InsertionEvent.WithResult>{
        @Alias("asStandaloneRule")
        public default IInsertionRule asIndividualRule() {return asStandaloneRule();}
        
        public default IInsertionRule asStandaloneRule()
        {
            return (e) -> {
                var withResult = InsertionEvent.WithResult.of(e);
                applyNoRUnsafe(withResult);
                return withResult.getResult();
            };
        }
    }
}


