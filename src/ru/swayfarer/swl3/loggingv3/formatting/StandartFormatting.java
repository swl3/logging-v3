package ru.swayfarer.swl3.loggingv3.formatting;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.event.formatting.ExceptionHeaderFormatter;
import ru.swayfarer.swl3.loggingv3.event.formatting.FormattingLogHandler.ILogFormatRule;
import ru.swayfarer.swl3.loggingv3.event.formatting.FormattingLogHandler.LogFormattingEvent;
import ru.swayfarer.swl3.loggingv3.event.formatting.ExceptionStacktraceFormatter;
import ru.swayfarer.swl3.loggingv3.formatting.insertion.InsertionsLogFormattingRule;
import ru.swayfarer.swl3.observable.event.AbstractEvent;
import ru.swayfarer.swl3.string.StringUtils;

@SuppressWarnings("unchecked")
public class StandartFormatting {

    public static InsertionsLogFormattingRule insertionsRule = getInsertionLogFormatterRule();
    
    public static ILogFormatRule replace(@NonNull String template, @NonNull IFunction1<LogEvent, String> valueFun)
    {
        return (e) -> {
            var logEvent = e.getLogEvent();
            var newStr = StringUtils.orEmpty(valueFun.apply(logEvent));
            
            var logContent = logEvent.getContent();
            logContent.setContentStr(logContent.getContentStr().replace(template, newStr));
        };
    }
    
    public static ExceptionStacktraceFormatter getStacktraceFormatter(@NonNull String format)
    {
        var ret = new ExceptionStacktraceFormatter(format);
        
        var insertionFuns = ret.getInsertionFuns();
        StandartStacktraceInsertions.addAllTo(insertionFuns);

        return ret;
    }

    public static ExceptionHeaderFormatter getExceptionsHeaderFormatter(@NonNull String format)
    {
        var ret = new ExceptionHeaderFormatter(format);

        var insertionFuns = ret.getInsertionFuns();
        StandardExceptionsHeadersInsertions.addAllTo(insertionFuns);

        return ret;
    }
    
    public static InsertionsLogFormattingRule getInsertionLogFormatterRule()
    {
        var ret = new InsertionsLogFormattingRule();
        
        var insFuns = ret.insertionsReplacer.insertionFuns;

        StandartLogInsertions.addAllTo(insFuns);

        return ret;
    }

    public static <T extends AbstractEvent> T setFormatEvent(@NonNull AbstractEvent event, @NonNull LogFormattingEvent formattingEvent)
    {
        event.getCustomAttributes().put("formattingEvent", formattingEvent);
        return (T) event;
    }

    public static LogFormattingEvent getFormatEvent(@NonNull AbstractEvent event) 
    {
        return event.getCustomAttributes().getValue("formattingEvent");
    }

    public static <T extends AbstractEvent> T setLogEvent(@NonNull AbstractEvent event, @NonNull LogEvent logEvent)
    {
        event.getCustomAttributes().put("logEvent", logEvent);
        return (T) event;
    }

    public static LogEvent getLogEvent(@NonNull AbstractEvent event)
    {
        return event.getCustomAttributes().getValue("logEvent");
    }
}
