package ru.swayfarer.swl3.loggingv3.formatting;

import lombok.var;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.markers.Alias;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;
import ru.swayfarer.swl3.string.insertions.replacer.NoDeepInsertionsReplacer;

public class StandartLogInsertions {

    public static EventInserionRule loggerName() 
    {
        return StandardInsertionsPresets.replaceInLog(
                (e) -> String.valueOf(e.getLogEvent().getLogger().getLoggerInfo().getName()),
                "logger", "log", "lname"
        );
    }
    
    public static EventInserionRule thread() 
    {
        return StandardInsertionsPresets.replaceInLog(
                (e) -> Thread.currentThread().getName(),
                "thread", "currentThread", "t", "ct"
        );
    }
    
    public static EventInserionRule text() 
    {
        return StandardInsertionsPresets.replaceInLog(
                (e) -> {
                    var contentStr = e.getLogEvent().getContent().getContentStr();
                    return contentStr;
                },
                "text", "message", "msg", "m"
        );
    }
    
    public static EventInserionRule level() 
    {
        return StandardInsertionsPresets.replaceInLog(
                (e) -> e.getLogEvent().getLevel().getDisplayName(),
                "level", "l", "lvl"
        );
    }
    
    public static EventInserionRule date()
    {
        var dateFormattersCache = new ExtendedMap<String, SimpleDateFormat>().concurrent();
        
        return StandardInsertionsPresets.replaceInLog((formatEvent, insertionEvent) -> {
            var insertion = insertionEvent.getInsertion();

            var formatString = insertion.getArgOrDefault(
                    String.class,
                    "yyyy-MM-dd HH:mm:ss",
                    "format", "f"
            );

            var formatter = dateFormattersCache.get(formatString);

            if (formatter == null)
            {
                formatter = new SimpleDateFormat(formatString);
                dateFormattersCache.put(formatString, formatter);
            }

            insertionEvent.setResult(formatter.format(new Date()));
        }, "date", "d");
    }
    
    @Alias("from")
    public static EventInserionRule source()
    {
        return from();
    }
    
    public static EventInserionRule from()
    {
        var noDeepReplacer = new NoDeepInsertionsReplacer();
        
        var insertionsParser = noDeepReplacer.insertionsParser;
        insertionsParser.insertionStarts.setAll("~%");
        insertionsParser.insertionEnds.setAll("%");
        insertionsParser.argSplitter = ";";

        StandartStacktraceInsertions.addAllTo(noDeepReplacer.getInsertionFuns());
        
        return StandardInsertionsPresets.replaceInLog((formatEvent, insertionEvent) -> {
            var insertion = insertionEvent.getInsertion();
            
            var formatString = insertion.getArgOrDefault(
                    String.class,
                    "~%f%:~%ln%", 
                    "format", "f"
            );
            
            if (!StringUtils.isEmpty(formatString))
            {
                var source = formatEvent.getLogEvent().getSource().getSource();
                var resultStr =  StandartStacktraceInsertions.formatStacktraceElement(noDeepReplacer, formatString, source);
                
                insertionEvent.setResult(resultStr);
            }
        }, "from", "caller", "source");
    }

    public static void addAllTo(FunctionsChain1<InsertionEvent, String> insFuns)
    {
        insFuns.add(StandartLogInsertions.loggerName());
        insFuns.add(StandartLogInsertions.thread());
        insFuns.add(StandartLogInsertions.level());
        insFuns.add(StandartLogInsertions.text(), 1);
        insFuns.add(StandartLogInsertions.date());
        insFuns.add(StandartLogInsertions.from());
    }
}
