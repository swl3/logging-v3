package ru.swayfarer.swl3.loggingv3.formatting;

import lombok.var;
import lombok.NonNull;
import lombok.SneakyThrows;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.chain.FunctionsChain1;
import ru.swayfarer.swl3.loggingv3.event.formatting.ExceptionStacktraceFormatter;
import ru.swayfarer.swl3.reflection.ClassLocationHelper;
import ru.swayfarer.swl3.reflection.ClassUtil;
import ru.swayfarer.swl3.reflection.ReflectionsUtils;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;
import ru.swayfarer.swl3.string.insertions.Insertion;
import ru.swayfarer.swl3.string.insertions.InsertionEvent;
import ru.swayfarer.swl3.string.insertions.InsertionsReplaceEvent;
import ru.swayfarer.swl3.string.insertions.replacer.NoDeepInsertionsReplacer;
import ru.swayfarer.swl3.thread.ThreadsUtils;

public class StandartStacktraceInsertions {
    
    public static EventInserionRule source() 
    {
        var cacheMapFun = ThreadsUtils.threadLocal(() -> new ExtendedMap<String, ExtendedMap<String, String>>());
        
        return StandardInsertionsPresets.replaceInStacktrace(
                (insEvent, elem) -> {
                    var insertion = insEvent.getInsertion();
                    var sourceFormat = insertion.getArgString("f", "format").orElse("%file%");
                    return formatStacktraceElementSource(sourceFormat, elem, cacheMapFun.apply());
                },
                "source", "container"
        );
    }
    
    public static EventInserionRule sourceCodeFile() 
    {
        return StandardInsertionsPresets.replaceInStacktrace(
                (elem) -> elem.getFileName(),
                "file", "f", "filename", "fname"
        );
    }
    
    public static EventInserionRule methodName() 
    {
        return StandardInsertionsPresets.replaceInStacktrace(
                (elem) -> elem.getMethodName(),
                "method", "methodName", "m"
        );
    }
    
    public static EventInserionRule lineNumber()
    {
        return StandardInsertionsPresets.replaceInStacktrace(
                (elem) -> String.valueOf(elem.getLineNumber()),
                "ln", "line"
        );
    }

    public static EventInserionRule exception()
    {
        return StandardInsertionsPresets.replaceByThrowable((insertionEvent, throwable) -> {
            return StringUtils.orEmpty(throwable);
        }, "e", "ex", "exception", "throwable", "throw");
    }
    
    public static EventInserionRule className() 
    {
        return StandardInsertionsPresets.replaceInStacktrace(
                (insEvent, elem) -> abbreviateClassName(insEvent.getInsertion(), elem.getClassName()),
                "class", "cl", "cls", "c"
        );
    }

    public static String abbreviateClassName(Insertion insertion, String className)
    {
        var packageElementLength = insertion.getArg(Double.class, "pkgLength", "pl", "psize", "ps").orNull();
        var type = insertion.getArgString("type", "t").orNull();

        if (packageElementLength != null && packageElementLength > 0)
        {
            className = ReflectionsUtils.getShortedClassName(className, packageElementLength.intValue());
        }

        if (type != null)
        switch (type)
        {
            case "simple":
            case "short":
            case "s":
            {
                className = ReflectionsUtils.getClassSimpleName(className);
                break;
            }

            case "package":
            case "pkg":
            case "p":
            {
                className = ReflectionsUtils.getPackageName(className);
                break;
            }
        }

        return className;
    }

    @SneakyThrows
    public static String formatStacktraceElementSource(@NonNull String format, @NonNull StackTraceElement elem, @NonNull ExtendedMap<String, ExtendedMap<String, String>> cache)
    {
        var className = elem.getClassName();
        var formatsCache = cache.get(format);
        
        if (formatsCache == null)
        {
            formatsCache = new ExtendedMap<>();
            cache.put(format, formatsCache);
        }
        
        var ret = formatsCache.get(className);
        
        if (ret == null)
        {
            var elemClass = ClassUtil.forName(className);
            var classLocation = ClassLocationHelper.findClassLocation(elemClass);
            
            ret = format
                    .replace("%file%", classLocation.getContainerSimpleName())
                    .replace("%entry%", StringUtils.orEmpty(classLocation.getPath()))
                    .replace("%url%", StringUtils.orEmpty(classLocation.getContainerLocation()))
            ;
            
            formatsCache.put(className, ret);
        }
        
        return ret;
    }

    public static String formatStacktraceElement(
            @NonNull NoDeepInsertionsReplacer insertionsReplacer,
            @NonNull String format,
            @NonNull StackTraceElement elem
    )
    {
        var newInsertionsEvent = InsertionsReplaceEvent.builder()
                .replaceString(format)
                .build()
        ;

        ExceptionStacktraceFormatter.setStacktraceElement(newInsertionsEvent, elem);
        return insertionsReplacer.replace(newInsertionsEvent);
    }
    
    public static FunctionsChain1<InsertionEvent, String> addAllTo(@NonNull FunctionsChain1<InsertionEvent, String> chain)
    {
        chain.add(className());
        chain.add(lineNumber());
        chain.add(methodName());
        chain.add(source());
        chain.add(sourceCodeFile());
        chain.add(exception());

        return chain;
    }
}
