package ru.swayfarer.swl3.loggingv3.formatting;

import lombok.var;
import lombok.NonNull;
import ru.swayfarer.swl3.loggingv3.coloring.ColoringManager;
import ru.swayfarer.swl3.loggingv3.formatting.StandardInsertionsPresets.IInsertionRulePart;
import ru.swayfarer.swl3.loggingv3.formatting.insertion.ColoringInsertionHandler;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.insertions.EventInserionRule;

public class StandartStringFormatters {

    public static int limitRulePriority = 1000;
    public static int caseRulePriority = 1100;
    
    public static IInsertionRulePart limitMaxLength()
    {
        return (event) -> {
            var result = event.getResult();
            
            if (!StringUtils.isEmpty(result))
            {
                var maxLength = event.getInsertion().getArg(
                        Double.class, 
                        "len", "l", "length", "maxLen"
                ).orNull();
                
                if (maxLength != null && maxLength > 0) 
                {
                    var length = result.length();
                    
                    if (length > maxLength)
                    {
                        event.setResult(result.substring(0, maxLength.intValue()));
                    }
                }
            }   
        };
    }
    
    public static IInsertionRulePart changeCase()
    {
        return (event) -> {
            var result = event.getResult();
            
            if (!StringUtils.isEmpty(result))
            {
                var caseString = event.getInsertion().getArg(
                        String.class, 
                        "case"
                ).orNull();
                
                if (!StringUtils.isEmpty(caseString))
                {
                    switch (caseString)
                    {
                        case "upper":
                        case "up":
                        case "u":
                        {
                            event.setResult(result.toUpperCase());
                            break;
                        }
                        case "lower":
                        case "low":
                        case "l":
                        {
                            event.setResult(result.toLowerCase());
                            break;
                        }
                    }
                }
            }
        };
    }
    
    public static IInsertionRulePart colorize(@NonNull ColoringManager coloringManager, @NonNull String defaultPalettete)
    {
        return new ColoringInsertionHandler(coloringManager, defaultPalettete);
    }

    public static EventInserionRule defaultStringRules()
    {
        var ret = new EventInserionRule();
        ret.eventInsert.subscribe().by(limitMaxLength(), limitRulePriority);
        ret.eventInsert.subscribe().by(changeCase(), caseRulePriority);
        return ret;
    }
}
