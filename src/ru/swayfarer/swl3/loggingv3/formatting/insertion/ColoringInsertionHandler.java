package ru.swayfarer.swl3.loggingv3.formatting.insertion;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.CollectionsSWL;
import ru.swayfarer.swl3.collections.list.ExtendedList;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction0;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.loggingv3.coloring.ColoringManager;
import ru.swayfarer.swl3.loggingv3.event.LogEvent;
import ru.swayfarer.swl3.loggingv3.formatting.StandardInsertionsPresets;
import ru.swayfarer.swl3.loggingv3.formatting.StandardInsertionsPresets.IInsertionRulePart;
import ru.swayfarer.swl3.loggingv3.formatting.StandartFormatting;
import ru.swayfarer.swl3.string.StringUtils;
import ru.swayfarer.swl3.string.dynamic.DynamicString;
import ru.swayfarer.swl3.string.insertions.InsertionEvent.WithResult;
import ru.swayfarer.swl3.thread.ThreadsUtils;

@Getter
@Setter
@Accessors(chain = true)
public class ColoringInsertionHandler implements IInsertionRulePart{

    @NonNull
    public ColoringManager coloringManager;
    
    @NonNull
    public String defaultPalette;
    
    @NonNull
    public IFunction0<History> history = ThreadsUtils.threadLocal(History::new);
    
    @NonNull
    public IFunction0<ExtendedMap<String, String>> cachedColorPrefixes = ThreadsUtils.singleton(ExtendedMap::new);

    @NonNull
    public ExtendedMap<String, String> levelColors = new ExtendedMap<>();

    public ColoringInsertionHandler(@NonNull ColoringManager coloringManager, @NonNull String defaultPalette)
    {
        this.coloringManager = coloringManager;
        this.defaultPalette = defaultPalette;
    }

    @Override
    public void applyNoRUnsafe(WithResult insertionEvent) throws Throwable
    {
        var history = getHistory();
        var insertion = insertionEvent.getInsertion();
        
        var palette = insertion.getArgString("p", "palette").orElse(defaultPalette);
        
        var fg = insertion.getArgString("fg", "foreground").orElse("");
        var bg = insertion.getArgString("bg", "background").orElse("");
        var style = insertion.getArgString("st", "style").orElse("");
        var resetPrev = insertion.getArgString("reset", "rs", "r").orElse("false");

        var str = new DynamicString();

        if ("level".equalsIgnoreCase(fg))
        {
            var logEvent = StandartFormatting.getLogEvent(insertionEvent);

            if (logEvent != null) {
                fg = levelColors.get(logEvent.getLevel().getName());
            }
        }

        if (Boolean.TRUE.equals(Boolean.valueOf(resetPrev))) {
            str.append("\033[0m");
        }

        append(palette, str, "style", style, history::getStyle, history::style, true);
        append(palette, str, "bg", bg, history::getBg, history::bg, false);
        append(palette, str, "fg", fg, history::getFg, history::fg, false);


        if (!StringUtils.isEmpty(str))
        {
            insertionEvent.setResult(str.toString());
        }
    }
    
    public void append(String palette, DynamicString str, String prefix, String color, IFunction1<Integer, String> historyGetFun, IFunction1<String, Object> historyUpdateFun, boolean allowMultuple)
    {
        if (color.trim().startsWith("h:"))
        {
            var split = color.split(":");
            if (split.length == 2)
            {
                var offset = StringUtils.strip(split[1]);
                
                if (StringUtils.isInteger(offset))
                {
                    var valueFromHistory = historyGetFun.apply(Integer.valueOf(offset));
                    
                    if (valueFromHistory != null)
                    {
                        str.append(valueFromHistory);
                        historyUpdateFun.apply(valueFromHistory);
                        return;
                    }
                }
            }
        }
        
        if (!StringUtils.isEmpty(color))
        {
            var cache = getCache();
            
            var colorsStr = cache.get(color);
            
            if (colorsStr == null)
            {
                var colorsStrBuffer = new DynamicString();
                
                var keys = allowMultuple ? 
                        CollectionsSWL.list(color.split(";")).map(StringUtils::strip)
                            : 
                        CollectionsSWL.list(color)
                ;
                
                for (var key : keys)
                {
                    var colorPrefix = coloringManager.getColorPrefix(palette, prefix + "_" + key);
                    
                    if (!StringUtils.isEmpty(colorPrefix))
                    {
                        colorsStrBuffer.append(colorPrefix);
                        historyUpdateFun.apply(colorPrefix);
                    }
                }
                
                colorsStr = colorsStrBuffer.toString();
                cache.put(color, colorsStr);
            }
            
            str.append(colorsStr);
        }
    }
    
    public ExtendedMap<String, String> getCache()
    {
        return cachedColorPrefixes.apply();
    }
    
    public History getHistory()
    {
        return history.apply();
    }
    
    @Data
    @Accessors(chain = true)
    public static class History {
        
        @NonNull
        public ExtendedList<String> styleHistory = new ExtendedList<>();
        
        @NonNull
        public ExtendedList<String> bgHistory = new ExtendedList<>();
        
        @NonNull
        public ExtendedList<String> fgHistory = new ExtendedList<>();
        
        public String getBg(int offset)
        {
            return getHistory(bgHistory, offset);
        }

        public String getFg(int offset)
        {
            return getHistory(fgHistory, offset);
        }

        public String getStyle(int offset)
        {
            return getHistory(styleHistory, offset);
        }
        
        public String getHistory(ExtendedList<String> list, int offset)
        {
            if (offset < 0 || offset >= list.size())
            {
                return "";
            }
            
            return list.get(list.size() - offset - 1);
        }
        
        public History bg(@NonNull String bgPrefix) 
        {
            bgHistory.add(bgPrefix);
            return this;
        }
        
        public History fg(@NonNull String fgPrefix) 
        {
            fgHistory.add(fgPrefix);
            return this;
        }
        
        public History style(@NonNull String stylePrefix) 
        {
            styleHistory.add(stylePrefix);
            return this;
        }
        
        public History reset()
        {
            clear();
            
            fg("\033[39m");
            bg("\033[49m");
            style("\033[0m");
            
            return this;
        }
        
        public History clear()
        {
            fgHistory.clear();
            bgHistory.clear();
            styleHistory.clear();
            
            return this;
        }
    }
    
    @Data
    @Accessors(chain = true)
    public static class HistotyEntry {
        
        @NonNull
        public String lastStylePrefix = "\033[0m";
        
        @NonNull
        public String lastForegroundPrefix = "\033[39m";
        
        @NonNull
        public String lastBackgroundPrefix = "\033[49m";
    }
}
