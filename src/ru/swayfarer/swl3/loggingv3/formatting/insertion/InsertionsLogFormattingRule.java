package ru.swayfarer.swl3.loggingv3.formatting.insertion;

import lombok.Getter;
import lombok.Setter;
import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.loggingv3.event.formatting.FormattingLogHandler.ILogFormatRule;
import ru.swayfarer.swl3.loggingv3.event.formatting.FormattingLogHandler.LogFormattingEvent;
import ru.swayfarer.swl3.loggingv3.formatting.StandartFormatting;
import ru.swayfarer.swl3.observable.IObservable;
import ru.swayfarer.swl3.observable.Observables;
import ru.swayfarer.swl3.string.insertions.InsertionsReplaceEvent;
import ru.swayfarer.swl3.string.insertions.replacer.NoDeepInsertionsReplacer;

@Getter
@Setter
@Accessors(chain = true)
public class InsertionsLogFormattingRule implements ILogFormatRule{

    @NonNull
    public IObservable<LogFormattingEvent> eventPreFormat = Observables.createObservable();
    
    @NonNull
    public IObservable<LogFormattingEvent> eventPostFormat = Observables.createObservable();
    
    @NonNull
    public NoDeepInsertionsReplacer insertionsReplacer = new NoDeepInsertionsReplacer();
    
    @Override
    public void applyNoRUnsafe(LogFormattingEvent formattingEvent) throws Throwable
    {
        eventPreFormat.next(formattingEvent);
        
        var replaceEvent = InsertionsReplaceEvent.builder()
                .replaceString(formattingEvent.getFormattingInfo().getFormat())
                .build()
        ;

        StandartFormatting.setLogEvent(replaceEvent, formattingEvent.getLogEvent());
        StandartFormatting.setFormatEvent(replaceEvent, formattingEvent);
        
        var replace = insertionsReplacer.replace(replaceEvent);
        formattingEvent.setResultStr(replace);
        
        eventPostFormat.next(formattingEvent);
    }
}
