package ru.swayfarer.swl3.loggingv3.info;

import lombok.var;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LogFormattingInfo {
    
    @NonNull
    public String format;
}
