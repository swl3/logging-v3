package ru.swayfarer.swl3.loggingv3.info;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.observable.event.EventSource;

@Data
@Accessors(chain = true)
@Builder
@AllArgsConstructor @NoArgsConstructor
public class LoggerInfo {
    
    @NonNull
    public String name;
    
    @Builder.Default
    @NonNull
    public EventSource source = new EventSource();
    
    public LoggerInfo updateSource(int offset)
    {
        source = new EventSource(offset + 2);
        return this;
    }
}
