package ru.swayfarer.swl3.loggingv3.io;

import lombok.var;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import lombok.NonNull;
import ru.swayfarer.swl3.exception.ExceptionsUtils;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.loggingv3.event.io.LogFileWriterHandler;

public class StandartLogFileArchiveConditions {
    
    public static AtomicLong nextShutdownThreadId = new AtomicLong();
    
    public static LogFileWriterHandler onShutdown(@NonNull LogFileWriterHandler handler)
    {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            handler.archiveIfNeed(true);
        }, "LogFileShutdownHook-" + nextShutdownThreadId.getAndIncrement()));
        
        return handler;
    }
    
    public static LogFileWriterHandler setLastArchiveTime(@NonNull LogFileWriterHandler handler, long time)
    {
        AtomicLong attr = handler.customAttributes.getOrCreate("lastArchiveTime", AtomicLong::new);
        attr.set(time);
        return handler;
    }
    
    public static long getLastArchiveTime(@NonNull LogFileWriterHandler handler)
    {
        AtomicLong attr = handler.customAttributes.getValue("lastArchiveTime");
        return attr == null ? -1 : attr.get();
    }
    
    public static IFunction1<LogFileWriterHandler, Boolean> maxFileSizeCondition(long sizeInBytes)
    {
        ExceptionsUtils.If(sizeInBytes <= 0, IllegalArgumentException.class, "file size in bytes must be bigger than 0!");
        
        return (handler) -> {
            var file = handler.getLogFile();
            
            if (file.length() > sizeInBytes)
                return true;
            
            return false;
        };
    }
    
    public static IFunction1<LogFileWriterHandler, Boolean> onStart()
    {
        var isSaved = new AtomicBoolean();
        return (handler) -> {
            if (!isSaved.get())
            {
                isSaved.set(true);
                return true;
            }
            
            return false;
        };
    }
    
    public static IFunction1<LogFileWriterHandler, Boolean> delayCondition(long delay, @NonNull TimeUnit timeUnit)
    {
        ExceptionsUtils.If(delay <= 0, IllegalArgumentException.class, "delay must be bigger than 0!");
        var delayInMilisis = timeUnit.toMillis(delay);
        
        return (handler) -> {
            
            long lastTime = getLastArchiveTime(handler);
            var now = System.currentTimeMillis();
            
            if (lastTime == -1)
            {
                setLastArchiveTime(handler, now);
                return false;
            }
            
            if (now - lastTime > delayInMilisis) 
            {
                setLastArchiveTime(handler, now);
                return true;
            }
            
            return false;
        };
    }
}
