package ru.swayfarer.swl3.loggingv3.io;

import lombok.var;
import java.io.OutputStream;

import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1;
import ru.swayfarer.swl3.funs.GeneratedFuns.IFunction1NoR;
import ru.swayfarer.swl3.io.file.FileSWL;
import ru.swayfarer.swl3.io.streams.StreamsUtils;
import ru.swayfarer.swl3.loggingv3.event.io.LogFileWriterHandler;

public class StandartLogFileArchivers {

    public static IFunction1NoR<LogFileWriterHandler> toFile(IFunction1<FileSWL, OutputStream> outStreamFun, String filenameTemplate)
    {
        return (handler) -> {
            var file = handler.getLogFile();
            
            file.exceptionsHandler.safe(() -> {
                var archiveFileName = filenameTemplate
                    .replace(
                            "%logFileNameWithoutExt%", 
                            file.getNameWithoutExtension()
                    )
                    .replace(
                            "%logFileExt%", 
                            file.getExtension()
                    )
                    .replace(
                            "%logFileName%", 
                            file.getName()
                    );
                
                var archiveFile = FileSWL.of(archiveFileName);
                
                int next = 0;
                
                while (archiveFile.exists())
                {
                    archiveFile = FileSWL.of(archiveFileName + "." + (++ next));
                }
                
                var os = outStreamFun.apply(archiveFile);
                StreamsUtils.copyStream(file.in(), os, true, true);
            }, "Error while archiving file", file);
        };
    }
    
    public static IFunction1NoR<LogFileWriterHandler> removeFile()
    {
        return (handler) -> {
            var file = handler.getLogFile();
            file.remove();
        }; 
    }
}
