package ru.swayfarer.swl3.loggingv3.level;

import lombok.var;
import ru.swayfarer.swl3.collections.map.ExtendedMap;

public interface ILogLevel {

    public String getName();
    public String getDisplayName();
    public int getWeight();
    
    public ExtendedMap<String, Object> getCustomAttributes();
}
