package ru.swayfarer.swl3.loggingv3.level;

public interface ILogLevels {

    public ILogLevel LEVEL_DEV = LogLevelV3.builder()
            .name("dev")
            .displayName("Dev")
            .weight(2000)
            .build()
    ;
    
    public ILogLevel LEVEL_INFO = LogLevelV3.builder()
            .name("info")
            .displayName("Info")
            .weight(3000)
            .build()
    ;
    
    public ILogLevel LEVEL_WARNING = LogLevelV3.builder()
            .name("warn")
            .displayName("Warning")
            .weight(4000)
            .build()
    ;
    
    public ILogLevel LEVEL_ERROR = LogLevelV3.builder()
            .name("err")
            .displayName("Error")
            .weight(5000)
            .build()
    ;
    
    public ILogLevel LEVEL_FATAL = LogLevelV3.builder()
            .name("fatal")
            .displayName("Fatal")
            .weight(6000)
            .build()
    ;
    
}
