package ru.swayfarer.swl3.loggingv3.level;

import lombok.var;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import ru.swayfarer.swl3.collections.map.ExtendedMap;
import ru.swayfarer.swl3.string.StringUtils;

@Data
@Accessors(chain = true)
@AllArgsConstructor @NoArgsConstructor
@Builder
public class LogLevelV3 implements ILogLevel {

    public String name;
    public String displayName;
    public int weight;
    
    @Builder.Default
    public ExtendedMap<String, Object> customAttributes = new ExtendedMap<>();
    
    @Override
    public String getDisplayName()
    {
        return StringUtils.isBlank(displayName) ? name : displayName;
    }
}
